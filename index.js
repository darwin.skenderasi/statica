alert("starting statica");

const staticaContent = document.getElementById("staticaContent");
const progressBar = document.getElementById("progressBar");
const progressContainer = document.getElementById("progressContainer");

const loadingDuration = 5000;  

const progressDuration = loadingDuration * 0.8;
const increment = 1;      
const intervalTime = progressDuration / 100;

let currentProgress = 0;

const interval = setInterval(() => {
    currentProgress += increment;
    progressBar.style.width = `${currentProgress}%`;

    if (currentProgress >= 100) {
        clearInterval(interval);
    }
}, intervalTime);

function simulateLoadingTask() {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve("Task completed");
        }, loadingDuration);
    });
}

simulateLoadingTask().then(() => {
    progressContainer.style.display = "none";
    
    staticaContent.classList.remove("hidden");

    // Alert the user
    alert("Statica has started!");
});

function toggleDropdown() {
    const content = document.getElementById("dropdownContent");
    content.style.display = content.style.display === "block" ? "none" : "block";
}
