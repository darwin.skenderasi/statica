-- Function to spoof ping
local function spoofPing()
    while true do
        game:GetService("ReplicatedStorage"):WaitForChild("PingEvent"):FireServer(2)
        wait(0.5)
    end
end

-- Create GUI function
local function createGUI()
    -- Get the local player
    local player = game:GetService("Players").LocalPlayer

    -- Create ScreenGui
    local gui = Instance.new("ScreenGui")
    gui.Name = "TrollGUI"
    gui.ResetOnSpawn = false
    gui.Parent = player.PlayerGui

    -- Create Main Frame
    local mainFrame = Instance.new("Frame")
    mainFrame.Size = UDim2.new(0, 300, 0, 100)
    mainFrame.Position = UDim2.new(0.5, -150, 0.5, -50)
    mainFrame.BackgroundTransparency = 0.3
    mainFrame.BackgroundColor3 = Color3.fromRGB(0, 0, 0)
    mainFrame.BorderSizePixel = 0
    mainFrame.Parent = gui

    -- Create Spoof Ping Label
    local spoofPingLabel = Instance.new("TextLabel")
    spoofPingLabel.Size = UDim2.new(0.4, 0, 0, 30)
    spoofPingLabel.Position = UDim2.new(0.05, 0, 0.3, 0)
    spoofPingLabel.Text = "Spoof Ping"
    spoofPingLabel.TextColor3 = Color3.fromRGB(255, 255, 255)
    spoofPingLabel.BackgroundTransparency = 1
    spoofPingLabel.Font = Enum.Font.SourceSansBold
    spoofPingLabel.TextSize = 16
    spoofPingLabel.Parent = mainFrame

    -- Create Toggle Switch
    local toggleSwitch = Instance.new("TextButton")
    toggleSwitch.Size = UDim2.new(0, 80, 0, 30)
    toggleSwitch.Position = UDim2.new(0.5, -40, 0.3, 0)
    toggleSwitch.Text = ""
    toggleSwitch.TextColor3 = Color3.fromRGB(255, 255, 255)
    toggleSwitch.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    toggleSwitch.BorderSizePixel = 0
    toggleSwitch.AutoButtonColor = false
    toggleSwitch.Parent = mainFrame

    -- Create Switch Handle
    local switchHandle = Instance.new("Frame")
    switchHandle.Size = UDim2.new(0.5, 0, 1, 0)
    switchHandle.Position = UDim2.new(0, 0, 0, 0)
    switchHandle.BackgroundColor3 = Color3.fromRGB(128, 128, 128)
    switchHandle.BorderSizePixel = 0
    switchHandle.Parent = toggleSwitch

    -- Functionality when Toggle Switch is clicked
    toggleSwitch.MouseButton1Click:Connect(function()
        if switchHandle.Position.X.Scale == 0 then
            switchHandle.Position = UDim2.new(0.5, 0, 0, 0)
            -- Enable spoofing
            spoofPing()
        else
            switchHandle.Position = UDim2.new(0, 0, 0, 0)
            -- Disable spoofing
            -- (You may want to add functionality to stop the spoofing)
        end
    end)
end

-- Execute the functions and create GUI
createGUI()
