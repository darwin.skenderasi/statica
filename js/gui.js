(function() {
    const guiContainer = document.createElement('div');
    guiContainer.id = 'gui-container';
    guiContainer.style.cssText = 'position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); width: 300px; height: 200px; background-color: #f1f1f1; border: 1px solid #ccc; border-radius: 5px; padding: 20px; display: none;';
    document.body.appendChild(guiContainer);
  
    fetch('https://reloaded-mu.vercel.app/js/gui.html')
      .then(response => response.text())
      .then(html => {
        guiContainer.innerHTML = html;
      })

      .catch(error => {
        alert('Something went wrong.', error);
      });
  
    document.addEventListener('keydown', (event) => {
      if (event.key === 'Shift' && event.location === 1) {
        guiContainer.style.display = guiContainer.style.display === 'none' ? 'block' : 'none';
      }
    });
  })();
  