// Creating the timer functionality and display
let startTime;
let interval;

function updateTimer() {
  const elapsedTime = Date.now() - startTime;
  const hours = Math.floor(elapsedTime / 3600000);
  const minutes = Math.floor((elapsedTime % 3600000) / 60000);
  const seconds = Math.floor((elapsedTime % 60000) / 1000);
  const milliseconds = elapsedTime % 1000;
  
  const formattedTime = `${hours.toString().padStart(2, '0')}:${minutes.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}.${milliseconds.toString().padStart(3, '0')}`;
  document.getElementById('timer').innerText = formattedTime;
}

function startTimer() {
  startTime = Date.now() - (interval || 0);
  interval = setInterval(updateTimer, 10); // Update every 10 milliseconds
}

function stopTimer() {
  clearInterval(interval);
  interval = null;
}

function restartTimer() {
  stopTimer();
  startTimer();
}

// Create and style the timer display element
const timerDisplay = document.createElement('div');
timerDisplay.id = 'timer';
timerDisplay.style.position = 'fixed';
timerDisplay.style.top = '10px';
timerDisplay.style.right = '10px';
timerDisplay.style.fontSize = '36px';
timerDisplay.style.fontFamily = 'Arial, sans-serif';
timerDisplay.style.color = '#00FF00'; // Green color
timerDisplay.style.background = '#000';
timerDisplay.style.padding = '5px 10px';
document.body.appendChild(timerDisplay);

// Event listeners for keyboard shortcuts
document.addEventListener('keydown', function(event) {
  if (event.shiftKey && event.key === '@') {
    startTimer();
  } else if (event.shiftKey && event.key === '#') {
    stopTimer();
  } else if (event.shiftKey && event.key === '!') {
    restartTimer();
  }
});
