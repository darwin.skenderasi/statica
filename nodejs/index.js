const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');

const app = express();
const port = 3000;

app.use(express.static('public')); // Serve static files from the 'public' folder

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Route to handle login requests
app.post('/login', (req, res) => {
  const { username, password } = req.body;

  // Read user data from the JSON file
  fs.readFile('users.json', (err, data) => {
    if (err) {
      console.error(err);
      return res.status(500).send('Error reading file');
    }

    const users = JSON.parse(data);
    const user = users.find(user => user.username === username && user.password === password);

    if (user) {
      res.send('Login successful!');
    } else {
      res.status(401).send('Invalid username or password');
    }
  });
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
