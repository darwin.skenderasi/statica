let startTime;
let updatedTime;
let difference = 0;
let tInterval;
let running = false;
let readyToStart = false;
let timerElement = document.getElementById('timer');
let scrambleElement = document.getElementById('scramble');
let bestTimeElement = document.getElementById('bestTime');
let averageTimeElement = document.getElementById('averageTime');
let solvesElement = document.getElementById('solves');
let solves = [];

function generateScramble() {
  // Scramble generation logic (same as before)
}

function startTimer() {
  if (!running) {
    startTime = Date.now() - difference;
    tInterval = setInterval(getShowTime, 10);
    running = true;
    timerElement.classList.remove('ready');
  }
}

function stopTimer() {
  clearInterval(tInterval);
  running = false;
  let finalTime = timerElement.innerHTML;
  solves.push(finalTime);
  updateStats();
  displaySolves();
  scrambleElement.innerHTML = generateScramble();
}

function resetTimer() {
  clearInterval(tInterval);
  running = false;
  difference = 0;
  timerElement.innerHTML = '00:00.000';
}

// Other functions (time conversions, stats update, etc.) remain the same

function getShowTime() {
  updatedTime = Date.now();
  difference = updatedTime - startTime;

  let minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
  let seconds = Math.floor((difference % (1000 * 60)) / 1000);
  let milliseconds = Math.floor((difference % (1000)));

  minutes = pad(minutes, 2);
  seconds = pad(seconds, 2);
  milliseconds = pad(milliseconds, 3);

  timerElement.innerHTML = `${minutes}:${seconds}.${milliseconds}`;
}

document.addEventListener('keydown', (event) => {
  if (event.code === 'Space' && !event.repeat) {
    if (!running && !readyToStart) {
      readyToStart = true;
      timerElement.classList.add('ready');
    }
  }
});

document.addEventListener('keyup', (event) => {
  if (event.code === 'Space') {
    if (!running && readyToStart) {
      startTimer();
    } else if (running) {
      stopTimer();
    }
    readyToStart = false;
    timerElement.classList.remove('ready');
  }
});

// Rest of the code (initial scramble setup, etc.) remains unchanged
scrambleElement.innerHTML = generateScramble(); // Initial scramble
